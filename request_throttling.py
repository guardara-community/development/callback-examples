# =============================================================================
# Example to demonstrate the use of the `runtime` methods exposed via the
# `context` argument of the callback function.
#
# This callback implements a basic request throttler that kicks in if the 
# Engine produces more than `max_tests_per_second` (default `100`) test cases 
# per second.
#
# Check the detailed test status page in Manager to see the messages popping up
# in the Activity Log of the test. One thing you will notice is that the 
# throttling is not perfectly accurate, even though very close to what was 
# expected. This is because the number of test cases generated and delivered 
# per second varies based on several factors, one being the length of the test 
# cases. This has an impact on the calculation of the average test cases per 
# second.
# =============================================================================

import time
from math import floor, ceil

def callback(context):
    # We do not want to throttle requests if the Engine is doing an issue 
    # analysis of performing performance baseline collection. 
    if context.runtime.is_analysis():
        return
    max_tests_per_second = 100
    speed = ceil(context.runtime.performance().get("average_test_cases_per_second"))
    if speed <= max_tests_per_second:
        return
    times = speed / max_tests_per_second
    # We are printing some information here just to demonstrate the use of the
    # log() method. This is tipically something you would NOT want to do. It is
    # here only for demonstration purposes.
    context.runtime.log(
        "info",
        f"Throttling requests. We were {round(times, 2)}x faster than expected, averaging {speed} test cases / second, instead of {max_tests_per_second}."
    )
    time.sleep(1 / floor(times))
