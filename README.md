# Callback Examples

This repository is a collection of GUARDARA Callbacks demonstrating the use of the different methods exposed via the `context` argument. Make sure to check out the documentation to learn more about how to use Callbacks.

## Examples

| Example      | Description | File     |
| ------------ | ----------- | -------- |
| Request Throttling | The example demonstrates how to use the `runtime` methods exposed via the `context` argument of the callback function. | [request_throttling.py](./request_throttling.py) |
